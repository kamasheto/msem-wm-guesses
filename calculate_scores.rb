
PREDICTIONS = <<-GOOGLE_SPREADSHEET_INFO

0:0 1:0 2:1 2:0 - - - -
2:3 0:4 0:3 0:5 0:4 1:3 0:3 1:2
0:2 3:1 2:1 3:2 2:2 2:1 2:0 2:2
0:4 1:3 1:2 0:2 2:0 0:2 1:2 0:2


GOOGLE_SPREADSHEET_INFO




def calculate_scores
  scores = nil
  
  puts "Individual game points:"
  puts "---------------------"

  PREDICTIONS.each_line do |game|
    game_preds = game.split
    next if game_preds.empty?

    final = game_preds.shift
    scores = Array.new(game_preds.count, 0) unless scores
    game_preds.each_with_index do |p, idx|
      if p == '-'
        pts = 0
      elsif p == final
        pts = 3
      elsif delta(p) == delta(final)
        pts = 2
      elsif winner(p) == winner(final)
        pts = 1
      else
        pts = 0
      end

      print "%s " % pts

      scores[idx] += pts
    end
    puts ''
  end

  puts ''
  puts ''

  scores
end

def score p
  ss = p.split ':'
  ss.map &:to_i
end

def delta p
  home, away = score p
  home - away
end

def winner p
  home, away = score p

  return 1 if home > away
  return 0 if home == away
  return -1 if home < away
end

final = calculate_scores
puts "Final score:"
puts "------------"
puts final.join ' '